#include <fstream>

std::string load(const std::string& filename, bool binary = false) {
	std::ifstream ifs(filename, binary ? std::ios::in | std::ios::binary : std::ios::in);
	if (!ifs) {throw std::runtime_error(filename + " could not be read");}
	std::istreambuf_iterator<char> begin(ifs), end;
	return std::string(begin, end);
}

int main(){
	printf("%s\n", load("CMakeCache.txt").c_str());
	return 0;
}
