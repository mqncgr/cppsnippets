#include <iostream>

// echo HelloWorld | ./read_input_stream

int main() {
	std::istreambuf_iterator<char> begin(std::cin), end;
	std::string input(begin, end);

	printf("%s\n", input.c_str());
	return 0;
}
