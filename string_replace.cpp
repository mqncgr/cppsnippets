#include <string>

void replaceInplace(
	std::string& str,
	const std::string& search,
	const std::string& repl
) {
	size_t start_pos = 0;
	while ((start_pos = str.find(search, start_pos)) != std::string::npos) {
		str.replace(start_pos, search.length(), repl);
		start_pos += repl.length();
	}
}

std::string replace(
	std::string str, // copied here
	const std::string& search,
	const std::string& repl
) {
	replaceInplace(str, search, repl);
	return str;
}

int main() {
	std::string s1 = "Blaukraut bleibt Blaukraut";
	std::string s2 = replace(s1, "Blaukraut", "Brautkleid");
	printf("%s und %s.\n", s1.c_str(), s2.c_str());
	return 0;
}
