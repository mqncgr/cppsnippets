#include <fstream>

void save(const std::string& filename, const std::string& content) {
	std::ofstream ofs(filename);
	if (!ofs) {throw std::runtime_error(filename + " could not be written");}
	ofs << content;
}

int main(){
	save("myFile.txt", "tabs are better than spaces");
	return 0;
}
