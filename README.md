# CppSnippets

handy snippets for programming C++ that should be one-liners in the standard library

## Save String to File

```cpp
#include <fstream>

void save(const std::string& filename, const std::string& content) {
	std::ofstream ofs(filename);
	if (!ofs) {throw std::runtime_error(filename + " could not be written");}
	ofs << content;
}
```

## Load Complete File into String

```cpp
#include <fstream>

std::string load(const std::string& filename, bool binary = false) {
	std::ifstream ifs(filename, binary ? std::ios::in | std::ios::binary : std::ios::in);
	if (!ifs) {throw std::runtime_error(filename + " could not be read");}
	std::istreambuf_iterator<char> begin(ifs), end;
	return std::string(begin, end);
}
```

## Read Complete Input Stream into String

```cpp
#include <iostream>

std::istreambuf_iterator<char> begin(std::cin), end;
std::string input(begin, end);
```

## Replace all Occurrences of String in other String

```cpp
#include <string>

void replaceInplace(
	std::string& str,
	const std::string& search,
	const std::string& repl
) {
	size_t start_pos = 0;
	while ((start_pos = str.find(search, start_pos)) != std::string::npos) {
		str.replace(start_pos, search.length(), repl);
		start_pos += repl.length();
	}
}

std::string replace(
	std::string str, // copied here
	const std::string& search,
	const std::string& repl
) {
	replaceInplace(str, search, repl);
	return str;
}
```
